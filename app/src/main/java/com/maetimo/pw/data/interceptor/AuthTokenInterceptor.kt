package com.maetimo.pw.data.interceptor

import android.util.Log
import com.maetimo.pw.data.auth.AuthManager
import com.maetimo.pw.data.exceptions.UnauthorizedException
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthTokenInterceptor @Inject constructor(private val authManager: AuthManager) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequestBuilder = chain.request().newBuilder()

        if (authManager.authToken != null) {
            Log.d(javaClass.simpleName, "Set auth token ${authManager.authToken}")
            newRequestBuilder.addHeader("Authorization", "Bearer ${authManager.authToken}")
        }

        try {
            return chain.proceed(newRequestBuilder.build())
        } catch (e: UnauthorizedException) {
            authManager.clearToken()
            throw e
        }
    }
}