package com.maetimo.pw.data

import com.maetimo.pw.data.dto.*
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    @POST("/sessions/create")
    fun login(@Body credentials: LoginRequestBodyDto): Single<AuthTokenDto>

    @POST("/users")
    fun register(@Body credentials: RegisterRequestBodyDto): Single<AuthTokenDto>

    @GET("/api/protected/user-info")
    fun getUserInfo(): Single<UserInfoResponseDto>

    @POST("/api/protected/users/list")
    fun getUserList(@Body filter: UserListRequestBodyDto): Single<List<UserListItemDto>>

    @GET("/api/protected/transactions")
    fun getTransactions(): Single<TransactionsResponseDto>

    @POST("/api/protected/transactions")
    fun createTransaction(@Body requestBody: TransactionCreateRequestDto): Single<TransactionCreateResponseDto>
}