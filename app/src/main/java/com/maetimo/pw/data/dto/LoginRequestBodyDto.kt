package com.maetimo.pw.data.dto

data class LoginRequestBodyDto(
    val email: String,
    val password: String
)