package com.maetimo.pw.data.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class TransactionCreateResponseDto(
    @JsonProperty("trans_token")
    val transactionToken: TransactionTokenDto
)