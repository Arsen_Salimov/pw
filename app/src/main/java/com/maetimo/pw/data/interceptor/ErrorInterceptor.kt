package com.maetimo.pw.data.interceptor

import com.maetimo.pw.data.exceptions.BadRequestException
import com.maetimo.pw.data.exceptions.InternalErrorException
import com.maetimo.pw.data.exceptions.UnauthorizedException
import okhttp3.Interceptor
import okhttp3.Response
import java.net.HttpURLConnection
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        if (!response.isSuccessful) {
            when (response.code()) {
                HttpURLConnection.HTTP_UNAUTHORIZED -> {
                    throw UnauthorizedException()
                }
                HttpURLConnection.HTTP_BAD_REQUEST -> {
                    throw BadRequestException(response.body()?.string())
                }
                HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                    throw InternalErrorException()
                }
            }
        }

        return response
    }
}