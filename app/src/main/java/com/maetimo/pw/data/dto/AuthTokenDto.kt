package com.maetimo.pw.data.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class AuthTokenDto(
    @JsonProperty("id_token")
    val token: String
)