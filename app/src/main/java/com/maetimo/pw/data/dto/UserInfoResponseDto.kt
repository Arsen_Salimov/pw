package com.maetimo.pw.data.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class UserInfoResponseDto(
    @JsonProperty("user_info_token")
    val userInfo: UserInfoDto
)