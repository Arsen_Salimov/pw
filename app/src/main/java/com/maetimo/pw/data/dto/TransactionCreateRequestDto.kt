package com.maetimo.pw.data.dto

data class TransactionCreateRequestDto(
    val name: String,
    val amount: Long
)