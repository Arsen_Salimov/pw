package com.maetimo.pw.data.dto

data class TransactionTokenDto(
    val id: String,
    val date: String,
    val username: String,
    val amount: Long,
    val balance: Int
)