package com.maetimo.pw.data.auth

import android.content.Context
import android.content.SharedPreferences
import com.maetimo.pw.R
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthManager
@Inject constructor(
    private val context: Context,
    private val sharedPreferences: SharedPreferences
) {
    var authToken: String? = loadToken()
        private set

    val isLoggedIn = authToken != null

    fun saveToken(token: String) {
        authToken = token
        saveTokenToSharedPrefs(authToken)
    }

    fun clearToken() {
        authToken = null
        //saveTokenToSharedPrefs(authToken)
    }

    private fun saveTokenToSharedPrefs(authToken: String?) {
        val authTokenKey = context.getString(R.string.authTokenKey)
        sharedPreferences
            .edit()
            .apply {
                if (authToken != null) {
                    putString(authTokenKey, authToken)
                } else {
                    remove(authTokenKey)
                }
            }
            .apply()
    }

    private fun loadToken(): String? {
        val authTokenKey = context.getString(R.string.authTokenKey)
        return sharedPreferences
            .getString(authTokenKey, null)
    }
}