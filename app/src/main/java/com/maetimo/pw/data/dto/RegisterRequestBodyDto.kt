package com.maetimo.pw.data.dto

data class RegisterRequestBodyDto(
    val username: String,
    val email: String,
    val password: String
)