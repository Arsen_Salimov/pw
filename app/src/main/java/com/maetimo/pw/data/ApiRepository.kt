package com.maetimo.pw.data

import com.maetimo.pw.data.auth.AuthManager
import com.maetimo.pw.data.dto.*
import com.maetimo.pw.data.exceptions.UnauthorizedException
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.HttpException
import javax.inject.Inject

class ApiRepository
@Inject constructor(
    private val apiService: ApiService,
    private val authManager: AuthManager
) {

    fun login(email: String, password: String): Single<AuthTokenDto> {
        return apiService.login(LoginRequestBodyDto(email = email, password = password))
            .doOnSuccess { authTokenDto -> authManager.saveToken(authTokenDto.token) }
    }

    fun register(username: String, password: String, email: String): Single<AuthTokenDto> {
        return apiService.register(
            RegisterRequestBodyDto(
                username = username,
                email = email,
                password = password
            )
        )
    }

    fun getUserList(filter: String): Single<List<UserListItemDto>> {
        return apiService.getUserList(UserListRequestBodyDto(filter))
    }

    fun getUserInfo(): Single<UserInfoDto> {
        return apiService.getUserInfo()
            .map { it.userInfo }
    }

    fun getTransactions(): Single<List<TransactionTokenDto>> {
        return apiService.getTransactions()
            .map { it.transactionTokens }
    }

    fun createTransaction(user: String, amount: Long): Single<TransactionTokenDto> {
        return apiService.createTransaction(TransactionCreateRequestDto(user, amount))
            .map { it.transactionToken }
    }
}