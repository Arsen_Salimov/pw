package com.maetimo.pw.data.exceptions

class BadRequestException(message: String?): ApiException(message)