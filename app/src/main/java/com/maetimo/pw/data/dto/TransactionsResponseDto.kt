package com.maetimo.pw.data.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class TransactionsResponseDto(
    @JsonProperty("trans_token")
    val transactionTokens: List<TransactionTokenDto>
)