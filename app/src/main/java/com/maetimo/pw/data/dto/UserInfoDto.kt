package com.maetimo.pw.data.dto

data class UserInfoDto(
    val id: Long,
    val name: String,
    val email: String,
    val balance: Long
)