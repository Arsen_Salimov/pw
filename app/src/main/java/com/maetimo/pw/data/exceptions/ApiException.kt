package com.maetimo.pw.data.exceptions

import java.lang.Exception

open class ApiException(message: String? = null): Exception(message)
