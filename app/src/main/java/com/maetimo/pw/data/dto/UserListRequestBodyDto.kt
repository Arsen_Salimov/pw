package com.maetimo.pw.data.dto

data class UserListRequestBodyDto(val filter: String)