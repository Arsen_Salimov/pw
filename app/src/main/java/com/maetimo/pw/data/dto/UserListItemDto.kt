package com.maetimo.pw.data.dto

data class UserListItemDto(
    val id: Long,
    val name: String
)