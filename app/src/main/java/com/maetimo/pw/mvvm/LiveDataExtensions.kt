import androidx.lifecycle.LiveData
import com.maetimo.pw.extensions.ZipListLiveData

object LiveDataExtensions {
    @Suppress("UNCHECKED_CAST")
    fun <T1, T2, R> zip(s1: LiveData<T1>, s2: LiveData<T2>, zipper: Function2<T1, T2, R>): LiveData<R> {

        val listZipper: Function1<List<Any?>, R> = { sources: List<Any?> ->
            zipper(
                sources[0] as T1,
                sources[1] as T2
            )
        }
        return zipList(listZipper, s1, s2)
    }

    @Suppress("UNCHECKED_CAST")
    fun <T1 : Any?, T2 : Any?, T3 : Any?, R> zip(
        s1: LiveData<T1>,
        s2: LiveData<T2>,
        s3: LiveData<T3>,
        zipper: Function3<T1, T2, T3, R>
    ): LiveData<R> {

        val listZipper = { sources: List<Any?> ->
            zipper(
                sources[0] as T1,
                sources[1] as T2,
                sources[2] as T3
            )
        }
        return zipList(listZipper, s1, s2, s3)
    }

    private fun <T : Any?, R> zipList(zipper: Function1<List<T?>, R>, vararg sources: LiveData<out T?>): LiveData<R> {
        return ZipListLiveData(sources.toList(), zipper)
    }
}
