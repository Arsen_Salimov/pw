package com.maetimo.pw.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

class ZipListLiveData<T: Any?, R>(
    private val sources: List<LiveData<out T>> = emptyList(),
    private val zipper: Function1<List<T?>, R>
) : LiveData<R>() {
    private val sourceObservers: Map<LiveData<out T>, Observer<T>>
    private val emittedFlags: MutableSet<LiveData<out T>> = mutableSetOf()

    init {
        sourceObservers = sources.map { source ->
            source to Observer<T> {
                emittedFlags.add(source)
                emitIfFilled()
            }
        }.toMap()
    }

    override fun onActive() {
        super.onActive()
        sourceObservers.entries.forEach { entry ->
            entry.key.observeForever(entry.value)
        }
    }

    override fun onInactive() {
        super.onInactive()

        sourceObservers.entries.forEach { entry ->
            entry.key.removeObserver(entry.value)
        }
    }

    private fun emitIfFilled() {
        if (emittedFlags.size == sources.size) {
            value = zipper(
                sources.map { it.value }
            )
        }
    }
}
