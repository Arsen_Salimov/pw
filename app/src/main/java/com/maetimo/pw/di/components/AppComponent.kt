package com.maetimo.pw.di.components

import com.maetimo.pw.App
import com.maetimo.pw.data.ApiModule
import com.maetimo.pw.di.builder.ActivityBuilderModule
import com.maetimo.pw.di.module.AppModule
import com.maetimo.pw.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        ApiModule::class,
        ViewModelModule::class,
        AndroidInjectionModule::class,
        ActivityBuilderModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}