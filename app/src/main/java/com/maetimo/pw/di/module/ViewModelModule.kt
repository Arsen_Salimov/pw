package com.maetimo.pw.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.maetimo.pw.ui.screen.auth.login.LoginViewModel
import com.maetimo.pw.ui.screen.auth.register.RegisterViewModel
import com.maetimo.pw.ui.base.ViewModelFactory
import com.maetimo.pw.ui.screen.create.TransactionCreateViewModel
import com.maetimo.pw.ui.screen.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @Suppress("unused")
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    @Suppress("unused")
    internal abstract fun loginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    @Suppress("unused")
    internal abstract fun registerViewModel(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    @Suppress("unused")
    internal abstract fun transactionListViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransactionCreateViewModel::class)
    @Suppress("unused")
    internal abstract fun transactionCreateViewModel(viewModel: TransactionCreateViewModel): ViewModel
}