package com.maetimo.pw.di.module

import android.content.Context
import com.maetimo.pw.App
import com.maetimo.pw.R
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(app: App): Context = app

    @Provides
    @Singleton
    fun provideSharedPrefs(app: App) =
        app.getSharedPreferences(app.getString(R.string.sharedPrefsName), Context.MODE_PRIVATE)!!
}