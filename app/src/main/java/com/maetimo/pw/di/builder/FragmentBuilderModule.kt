package com.maetimo.pw.di.builder

import com.maetimo.pw.ui.screen.auth.login.LoginFragment
import com.maetimo.pw.ui.screen.auth.register.RegisterFragment
import com.maetimo.pw.ui.screen.create.TransactionCreateFragment
import com.maetimo.pw.ui.screen.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {
    @Suppress("unused")
    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @Suppress("unused")
    @ContributesAndroidInjector
    abstract fun contributeRegisterFragment(): RegisterFragment

    @Suppress("unused")
    @ContributesAndroidInjector
    abstract fun contributeTransactionListFragment(): HomeFragment

    @Suppress("unused")
    @ContributesAndroidInjector
    abstract fun contributeTransactionCreateFragment(): TransactionCreateFragment
}