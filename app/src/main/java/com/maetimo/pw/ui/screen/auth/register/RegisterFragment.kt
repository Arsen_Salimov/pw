package com.maetimo.pw.ui.screen.auth.register

import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.transition.AutoTransition
import com.google.android.material.snackbar.Snackbar
import com.maetimo.pw.R
import com.maetimo.pw.data.dto.AuthTokenDto
import com.maetimo.pw.databinding.RegisterFragmentBinding
import com.maetimo.pw.ui.base.BaseFragment

class RegisterFragment : BaseFragment<RegisterViewModel, RegisterFragmentBinding>() {

    override fun getLayoutId(): Int = R.layout.register_fragment

    override fun initViews() {
        super.initViews()

        viewModel.registerResultLive.observe(this, Observer(this::handleRegisterResult))
        binding.registerBtn.setOnClickListener { viewModel.register() }
    }

    override fun initAnimationTransitions() {
        super.initAnimationTransitions()
        val animationDuration = resources.getInteger(R.integer.default_transition_animation_duration).toLong()

        enterTransition = AutoTransition().apply {
            duration = animationDuration / 2
        }
        exitTransition = AutoTransition().apply {
            duration = animationDuration / 2
        }
        sharedElementEnterTransition = AutoTransition().apply {
            duration = animationDuration
        }
        sharedElementReturnTransition = AutoTransition().apply {
            duration = animationDuration
        }
    }

    private fun handleRegisterResult(loginResult: Result<AuthTokenDto>) {
        if (loginResult.isSuccess) {
            onRegisterSuccess()
        } else {
            onRegisterSuccess(loginResult.exceptionOrNull())
        }
    }

    private fun onRegisterSuccess() {
        Navigation.findNavController(binding.root).popBackStack()
    }

    private fun onRegisterSuccess(error: Throwable?) {
        val errorMessage = error?.message ?: getString(R.string.unknown_error_message)
        Snackbar.make(binding.container, errorMessage, Snackbar.LENGTH_LONG).show()
    }
}
