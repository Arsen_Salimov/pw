package com.maetimo.pw.ui.screen.create

import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar

import com.maetimo.pw.R
import com.maetimo.pw.data.dto.UserListItemDto
import com.maetimo.pw.data.exceptions.BadRequestException
import com.maetimo.pw.data.exceptions.UnauthorizedException
import com.maetimo.pw.databinding.TransactionCreateFragmentBinding
import com.maetimo.pw.ui.base.BaseFragment

class TransactionCreateFragment : BaseFragment<TransactionCreateViewModel, TransactionCreateFragmentBinding>() {
    private val userListAdapter by lazy {
        ArrayAdapter<String>(context!!, R.layout.user_list_item, R.id.userNameTv).apply {
            setNotifyOnChange(false)
        }
    }

    override fun getLayoutId(): Int = R.layout.transaction_create_fragment

    override fun initViews() {
        binding.apply {
            createBtn.setOnClickListener { viewModel?.createTransaction() }
            binding.userSelectSp.setAdapter(userListAdapter)
        }

        viewModel.apply {
            usersLive.observe(this@TransactionCreateFragment, Observer(this@TransactionCreateFragment::setUsers))

            userFilterErrorsLive.observe(
                this@TransactionCreateFragment,
                Observer { binding.userSelectSpLayout.error = it })

            transactionCreateResult.observe(
                this@TransactionCreateFragment,
                Observer(this@TransactionCreateFragment::handleTransactionCreateResult)
            )
        }

        checkArgs()
    }

    private fun checkArgs() {
        arguments?.let {
            val args = TransactionCreateFragmentArgs.fromBundle(it)
            args.username?.let { username -> viewModel.fill(username, args.amount) }
        }
    }

    private fun setUsers(users: List<UserListItemDto>) {
        userListAdapter.apply {
            clear()
            addAll(users.map { it.name })
            notifyDataSetChanged()
        }
    }

    private fun handleTransactionCreateResult(transactionCreateResult: TransactionCreateResult) {
        when (transactionCreateResult) {
            is TransactionCreateResult.Success -> onLoginSuccess()
            is TransactionCreateResult.Fail -> onLoginError(transactionCreateResult.error)
        }
    }

    private fun onLoginSuccess() {
        Navigation.findNavController(binding.root).popBackStack()
    }

    private fun onLoginError(error: Throwable) {
        val errorMessage = when (error) {
            is UnauthorizedException -> getString(R.string.unauthorized_error_message)
            is BadRequestException -> error.message ?: getString(R.string.unknown_error_message)
            else -> getString(R.string.unknown_error_message)
        }
        Snackbar.make(binding.container, errorMessage, Snackbar.LENGTH_LONG).show()
    }
}
