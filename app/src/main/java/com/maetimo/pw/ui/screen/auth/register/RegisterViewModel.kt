package com.maetimo.pw.ui.screen.auth.register

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.maetimo.pw.R
import com.maetimo.pw.constants.Constants
import com.maetimo.pw.data.ApiRepository
import com.maetimo.pw.data.dto.AuthTokenDto
import com.maetimo.pw.mvvm.SingleLiveEvent
import com.maetimo.pw.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class RegisterViewModel
@Inject constructor(
    private val context: Context,
    private val apiRepository: ApiRepository
) : BaseViewModel() {
    val isRegisterInProgressLive = MutableLiveData<Boolean>(false)

    val usernameLive = MutableLiveData<String>()
    val emailLive = MutableLiveData<String>()
    val passwordLive = MutableLiveData<String>()

    val usernameErrorsLive: LiveData<String?> = Transformations.map(usernameLive, this::validateUsername)
    val emailErrorsLive: LiveData<String?> = Transformations.map(emailLive, this::validateEmail)
    val passwordErrorsLive: LiveData<String?> = Transformations.map(passwordLive, this::validatePassword)

    private val isFormValidLive: LiveData<Boolean> = LiveDataExtensions.zip(
        usernameErrorsLive,
        emailErrorsLive,
        passwordErrorsLive
    ) { usernameErrors, emailErrors, passwordErrors ->
        usernameErrors == null && emailErrors == null && passwordErrors == null
    }

    val isRegisterEnableLive = LiveDataExtensions.zip(
        isFormValidLive,
        isRegisterInProgressLive,
        this::isRegisterEnabled
    )

    val registerResultLive = SingleLiveEvent<Result<AuthTokenDto>>()

    fun register() {
        disposable.add(
            apiRepository
                .register(usernameLive.value!!, passwordLive.value!!, emailLive.value!!)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isRegisterInProgressLive.value = true }
                .doFinally { isRegisterInProgressLive.value = false }
                .subscribe(this::handleSuccess, this::handleError)
        )
    }

    private fun handleSuccess(authTokenDto: AuthTokenDto) {
        Log.d(javaClass.simpleName, "saveToken success")
        registerResultLive.value = Result.success(authTokenDto)
    }

    private fun handleError(error: Throwable) {
        Log.e(javaClass.simpleName, "error during register request", error)
        registerResultLive.value = Result.failure(error)
    }

    private fun validateUsername(email: String?): String? {
        return when {
            TextUtils.isEmpty(email) -> context.getString(R.string.username_empty_error_message)
            else -> null
        }
    }

    private fun validateEmail(email: String?): String? {
        return when {
            TextUtils.isEmpty(email) -> context.getString(R.string.email_empty_error_message)
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> context.getString(R.string.email_wrong_format_error_message)
            else -> null
        }
    }

    private fun validatePassword(password: String?): String? {
        return when {
            TextUtils.isEmpty(password) -> context.getString(R.string.password_empty_error_message)
            (password?.length ?: 0) < Constants.MIN_PASSWORD_LENGTH -> context.getString(
                R.string.password_min_length_error_message,
                Constants.MIN_PASSWORD_LENGTH
            )
            else -> null
        }
    }

    private fun isRegisterEnabled(isFormValid: Boolean, isRegisterInProcess: Boolean): Boolean {
        return isFormValid && !isRegisterInProcess
    }
}