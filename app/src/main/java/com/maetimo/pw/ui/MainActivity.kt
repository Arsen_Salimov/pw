package com.maetimo.pw.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.maetimo.pw.data.auth.AuthManager
import com.maetimo.pw.R
import com.maetimo.pw.databinding.MainActivityBinding
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import androidx.navigation.fragment.NavHostFragment

open class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {
    private lateinit var binding: MainActivityBinding

    @Inject
    protected lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    protected lateinit var authManager: AuthManager

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        setupNavigation(savedInstanceState)
    }

    private fun setupNavigation(savedInstanceState: Bundle?) {
        setSupportActionBar(binding.toolbar)

        val navHost = supportFragmentManager.findFragmentById(R.id.navHosFragment) as NavHostFragment?
        val navController = navHost!!.navController

        if (savedInstanceState == null) {
            val navInflater = navController.navInflater
            val graph = navInflater.inflate(com.maetimo.pw.R.navigation.main)
            graph.startDestination = getStartDestination()

            navController.graph = graph
        }

        val appBarConfiguration =
            AppBarConfiguration(setOf(com.maetimo.pw.R.id.loginFragment, com.maetimo.pw.R.id.homeFragment))

        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
    }

    @IdRes
    private fun getStartDestination(): Int {
        return if (authManager.isLoggedIn) R.id.homeFragment else R.id.loginFragment
    }
}
