package com.maetimo.pw.ui.screen.create

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import com.maetimo.pw.data.dto.UserListItemDto
import com.maetimo.pw.databinding.UserListItemBinding

class UserListAdapter : BaseAdapter(), Filterable {
    private val users: MutableList<UserListItemDto> = mutableListOf()

    fun setUsers(users: List<UserListItemDto>) {
        this.users.clear()
        this.users.addAll(users)
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val binding = UserListItemBinding.inflate(layoutInflater, parent, false)

        binding.userNameTv.text = users[position].name

        return binding.root
    }

    override fun getItem(position: Int): Any = users[position].name

    override fun getItemId(position: Int): Long = users[position].id

    override fun getCount(): Int = users.size

    override fun getFilter(): Filter = UserFilter()

    inner class UserFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filterResults = FilterResults()

            filterResults.count = users.size
            filterResults.values = users.map { it.name }

            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            notifyDataSetChanged()
        }
    }
}