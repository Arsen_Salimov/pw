package com.maetimo.pw.ui.screen.auth.login

import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.transition.AutoTransition
import com.google.android.material.snackbar.Snackbar
import com.maetimo.pw.R
import com.maetimo.pw.data.dto.AuthTokenDto
import com.maetimo.pw.data.exceptions.UnauthorizedException
import com.maetimo.pw.databinding.LoginFragmentBinding
import com.maetimo.pw.ui.base.BaseFragment

class LoginFragment : BaseFragment<LoginViewModel, LoginFragmentBinding>() {

    override fun getLayoutId(): Int = R.layout.login_fragment

    override fun initViews() {
        super.initViews()
        viewModel.loginResultLive.observe(this, Observer(this::handleLoginResult))

        binding.apply {
            loginBtn.setOnClickListener { viewModel?.login() }
            registerBtn.setOnClickListener(this@LoginFragment::onRegisterButtonClicked)
        }
    }

    private fun handleLoginResult(loginResult: Result<AuthTokenDto>) {
        if (loginResult.isSuccess) {
            onLoginSuccess()
        } else {
            onLoginError(loginResult.exceptionOrNull())
        }
    }

    private fun onLoginSuccess() {
        Navigation.findNavController(binding.root).navigate(LoginFragmentDirections.actionLoginToTransactionList())
    }

    @Suppress("UNUSED_PARAMETER")
    private fun onRegisterButtonClicked(view: View) {
        val animationDuration = resources.getInteger(R.integer.default_transition_animation_duration).toLong()

        enterTransition = AutoTransition().apply {
            duration = animationDuration / 2
        }
        exitTransition = AutoTransition().apply {
            duration = animationDuration / 2
        }
        sharedElementEnterTransition = AutoTransition().apply {
            duration = animationDuration
        }
        sharedElementReturnTransition = AutoTransition().apply {
            duration = animationDuration
        }

        val extras = FragmentNavigatorExtras(
            binding.emailEtLayout to getString(R.string.email_et_transition_name),
            binding.passwordEtLayout to getString(R.string.password_Et_transition_name),
            binding.registerBtn to getString(R.string.register_btn_transition_name)
        )
        Navigation.findNavController(binding.root).navigate(LoginFragmentDirections.actionLoginToRegister(), extras)
    }

    private fun onLoginError(error: Throwable?) {
        val errorMessage = when (error) {
            is UnauthorizedException -> getString(R.string.wrong_credentials_error_message)
            else -> getString(R.string.unknown_error_message)
        }
        Snackbar.make(binding.container, errorMessage, Snackbar.LENGTH_LONG).show()
    }
}
