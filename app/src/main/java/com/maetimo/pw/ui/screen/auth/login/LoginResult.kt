package com.maetimo.pw.ui.screen.auth.login

sealed class LoginResult {
    object Success : LoginResult()
    data class Fail(val error: Throwable): LoginResult()
}