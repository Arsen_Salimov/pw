package com.maetimo.pw.ui.screen.auth.login

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.maetimo.pw.R
import com.maetimo.pw.mvvm.SingleLiveEvent
import com.maetimo.pw.constants.Constants
import com.maetimo.pw.data.ApiRepository
import com.maetimo.pw.data.dto.AuthTokenDto
import com.maetimo.pw.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class LoginViewModel
@Inject constructor(
    private val context: Context,
    private val apiRepository: ApiRepository
) : BaseViewModel() {

    val isLoginInProgressLive = MutableLiveData<Boolean>(false)

    val emailLive = MutableLiveData<String>(null)
    val passwordLive = MutableLiveData<String>(null)

    val emailErrorsLive: LiveData<String?> = Transformations.map(emailLive, this::validateEmail)
    val passwordErrorsLive: LiveData<String?> = Transformations.map(passwordLive, this::validatePassword)

    private val isEmailValid: LiveData<Boolean> = LiveDataExtensions.zip(emailLive, emailErrorsLive, this::isFieldValid)
    private val isPasswordValid = LiveDataExtensions.zip(passwordLive, passwordErrorsLive, this::isFieldValid)

    private val isFormValidLive: LiveData<Boolean> = LiveDataExtensions.zip(
        isEmailValid,
        isPasswordValid
    ) { isEmailValid, isPasswordValid ->
        isEmailValid && isPasswordValid
    }

    val isLoginEnabledLive = LiveDataExtensions.zip(
        isFormValidLive,
        isLoginInProgressLive,
        this::isLoginEnabled
    )

    val loginResultLive = SingleLiveEvent<Result<AuthTokenDto>>()

    fun login() {
        disposable.add(
            apiRepository
                .login(emailLive.value!!, passwordLive.value!!)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isLoginInProgressLive.value = true }
                .doFinally { isLoginInProgressLive.value = false }
                .subscribe(this::onLoginSuccess, this::onLoginFailed)
        )
    }

    private fun onLoginSuccess(authTokenDto: AuthTokenDto) {
        Log.d(javaClass.simpleName, "saveToken success")
        loginResultLive.value = Result.success(authTokenDto)
    }

    private fun onLoginFailed(error: Throwable) {
        Log.e(javaClass.simpleName, "error during saveToken request", error)
        loginResultLive.value = Result.failure(error)
    }

    private fun validateEmail(email: String?): String? {
        return when {
            TextUtils.isEmpty(email) -> context.getString(R.string.email_empty_error_message)
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> context.getString(R.string.email_wrong_format_error_message)
            else -> null
        }
    }

    private fun validatePassword(password: String?): String? {
        return when {
            TextUtils.isEmpty(password) -> context.getString(R.string.password_empty_error_message)
            (password?.length ?: 0) < Constants.MIN_PASSWORD_LENGTH -> context.getString(
                R.string.password_min_length_error_message,
                Constants.MIN_PASSWORD_LENGTH
            )
            else -> null
        }
    }

    private fun isFieldValid(field: String?, error: String?): Boolean {
        return field != null && error == null
    }

    private fun isLoginEnabled(isFormValid: Boolean, isLoginInProcess: Boolean): Boolean {
        return isFormValid && !isLoginInProcess
    }
}