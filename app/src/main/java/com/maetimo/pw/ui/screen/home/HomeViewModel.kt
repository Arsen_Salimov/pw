package com.maetimo.pw.ui.screen.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.maetimo.pw.data.ApiRepository
import com.maetimo.pw.data.auth.AuthManager
import com.maetimo.pw.data.dto.TransactionTokenDto
import com.maetimo.pw.data.dto.UserInfoDto
import com.maetimo.pw.mvvm.SingleLiveEvent
import com.maetimo.pw.ui.base.BaseViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HomeViewModel
@Inject constructor(
    private val apiRepository: ApiRepository,
    private val authManager: AuthManager
) : BaseViewModel() {
    val userInfoLive = MutableLiveData<UserInfoDto>()
    val transactionsLive = MutableLiveData<List<TransactionTokenDto>>()
    val transactionsExistLive = Transformations.map(transactionsLive) { it.isNotEmpty() }
    val apiErrorsLive = SingleLiveEvent<Throwable>()

    fun logout() {
        authManager.clearToken()
    }

    fun loadData() {
        val transactionsRequest = apiRepository
            .getTransactions()
            .observeOn(AndroidSchedulers.mainThread())

        val userInfoRequest = apiRepository
            .getUserInfo()
            .observeOn(AndroidSchedulers.mainThread())

        disposable.add(
            Single.zip(
                transactionsRequest,
                userInfoRequest,
                BiFunction<List<TransactionTokenDto>, UserInfoDto, Pair<List<TransactionTokenDto>, UserInfoDto>> { transactions, userInfo ->
                    Pair(
                        transactions,
                        userInfo
                    )
                })
                .subscribe(
                    { result ->
                        transactionsLive.value = result.first
                        userInfoLive.value = result.second
                    },
                    { error -> apiErrorsLive.value = error }
                )
        )
    }
}