package com.maetimo.pw.ui.extensions

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputEditText

@BindingAdapter("error")
fun TextInputEditText.setError(errorLiveData: LiveData<String>) {
    error = errorLiveData.value
}

@BindingAdapter("visibleOrGone")
fun View.setIsVisibleOrGone(isVisible: Boolean) {
    visibility = if (isVisible) VISIBLE else GONE
}

@BindingAdapter("android:text")
fun TextInputEditText.setNumber(data: MutableLiveData<Long>) {
    val stringValue = data.value?.toString() ?: ""

    if (stringValue != text.toString()) {
        setText(stringValue)
    }
}

@InverseBindingAdapter(attribute = "android:text")
fun TextInputEditText.getNumber(): Long? {
    return text.toString().toLongOrNull()
}