package com.maetimo.pw.ui.screen.create

sealed class TransactionCreateResult {
    object Success : TransactionCreateResult()
    data class Fail(val error: Throwable): TransactionCreateResult()
}