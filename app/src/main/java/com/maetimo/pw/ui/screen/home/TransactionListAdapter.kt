package com.maetimo.pw.ui.screen.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maetimo.pw.data.dto.TransactionTokenDto
import com.maetimo.pw.databinding.TransactionListItemBinding

class TransactionListAdapter : RecyclerView.Adapter<TransactionListAdapter.ViewHolder>() {
    var repeatTransactionClickListener: (TransactionTokenDto) -> Unit = {}
    private val transactions: MutableList<TransactionTokenDto> = mutableListOf()

    fun addTransactions(transactions: List<TransactionTokenDto>) {
        this.transactions.clear()
        this.transactions.addAll(transactions)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = TransactionListItemBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = transactions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val transaction = transactions[position]
        holder.bind(transaction)
    }

    inner class ViewHolder(private val binding: TransactionListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(transaction: TransactionTokenDto) {
            binding.transaction = transaction
            binding.repeatBtn.setOnClickListener { repeatTransactionClickListener(transaction) }
            binding.executePendingBindings()
        }
    }
}