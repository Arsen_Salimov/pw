package com.maetimo.pw.ui.screen.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

import com.maetimo.pw.R
import com.maetimo.pw.data.dto.TransactionTokenDto
import com.maetimo.pw.data.exceptions.UnauthorizedException
import com.maetimo.pw.databinding.HomeFragmentBinding
import com.maetimo.pw.ui.base.BaseFragment

class HomeFragment : BaseFragment<HomeViewModel, HomeFragmentBinding>() {
    private val listAdapter = TransactionListAdapter()

    override fun getLayoutId(): Int = R.layout.home_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun initViews() {
        listAdapter.repeatTransactionClickListener = this::onRepeatTransactionClick
        binding.apply {
            recentTransactionsRv.apply {
                adapter = listAdapter
                layoutManager = LinearLayoutManager(context)
            }

            addTransBtn.setOnClickListener(this@HomeFragment::onAddTransClick)
        }

        viewModel.apply {
            transactionsLive.observe(this@HomeFragment, Observer(listAdapter::addTransactions))
            apiErrorsLive.observe(this@HomeFragment, Observer(this@HomeFragment::handleError))
            viewModel.loadData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.logout -> {
                logout()
                true
            }
            else -> false
        }
    }

    private fun handleError(error: Throwable) {
        when (error) {
            is UnauthorizedException -> {
                Snackbar.make(binding.container, getString(R.string.unauthorized_error_message), Snackbar.LENGTH_LONG)
                    .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                            super.onDismissed(transientBottomBar, event)
                            logout()
                        }
                    })
                    .show()
            }
            else -> Snackbar.make(
                binding.container,
                getString(R.string.unknown_error_message),
                Snackbar.LENGTH_LONG
            ).show()
        }
    }

    private fun logout() {
        viewModel.logout()
        Navigation.findNavController(binding.root)
            .navigate(HomeFragmentDirections.actionHomeToLogin())
    }

    private fun onRepeatTransactionClick(transaction: TransactionTokenDto) {
        Navigation.findNavController(binding.root)
            .navigate(HomeFragmentDirections.actionHomeToTransactionCreate(transaction.username, transaction.amount))
    }

    @Suppress("UNUSED_PARAMETER")
    private fun onAddTransClick(view: View) {
        Navigation.findNavController(binding.root)
            .navigate(HomeFragmentDirections.actionHomeToTransactionCreate(null, 0))
    }
}
