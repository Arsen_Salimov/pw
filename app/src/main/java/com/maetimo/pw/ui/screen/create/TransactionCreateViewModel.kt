package com.maetimo.pw.ui.screen.create

import android.content.Context
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.maetimo.pw.R
import com.maetimo.pw.data.ApiRepository
import com.maetimo.pw.data.dto.UserListItemDto
import com.maetimo.pw.ui.base.BaseViewModel
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class TransactionCreateViewModel
@Inject constructor(
    private val context: Context,
    private val apiRepository: ApiRepository
) : BaseViewModel() {
    val isCreateRequestInProcessLive = MutableLiveData<Boolean>(false)

    val amount = MutableLiveData<Long>()
    val userFilterLive = MutableLiveData<String>()
    val usersLive = Transformations.switchMap(userFilterLive) { LiveDataReactiveStreams.fromPublisher(loadUsers(it)) }
    val userFilterErrorsLive = Transformations.map(usersLive, this::validateUser)
    val amountErrorsLive = Transformations.map(amount, this::validateAmount)
    private val isFormValidLive =
        LiveDataExtensions.zip(userFilterErrorsLive, amountErrorsLive) { userFilterErrors, amountErrors ->
            userFilterErrors == null && amountErrors == null
        }
    val isCreateEnabled =
        LiveDataExtensions.zip(isFormValidLive, isCreateRequestInProcessLive) { isFormValid, isCreateRequestInProcess ->
            isFormValid && !isCreateRequestInProcess
        }
    val transactionCreateResult = MutableLiveData<TransactionCreateResult>()

    fun fill(username: String, amount: Long) {
        this.amount.value = amount
        this.userFilterLive.value = username
    }

    private fun loadUsers(filter: String): Flowable<List<UserListItemDto>> {
        // api bug. When filter is empty api returns 401 unauthorized and it breaks auth management flow
        // so to prevent it, block request with empty filter
        return if (filter.isNotEmpty()) {
            apiRepository
                .getUserList(filter)
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { emptyList() }
                .toFlowable()
        } else {
            Flowable.just(emptyList())
        }
    }

    fun createTransaction() {
        val amount = this.amount.value

        disposable.add(
            apiRepository
                .createTransaction(userFilterLive.value!!, amount!!)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isCreateRequestInProcessLive.value = true }
                .doFinally { isCreateRequestInProcessLive.value = false }
                .subscribe(
                    { transactionCreateResult.value = TransactionCreateResult.Success },
                    { transactionCreateResult.value = TransactionCreateResult.Fail(it) }
                )
        )
    }

    private fun validateUser(users: List<UserListItemDto>): String? {
        if (users.size != 1) {
            return context.getString(R.string.empty_user_error_message)
        }

        return null
    }

    private fun validateAmount(amount: Long?): String? {
        if (amount == null) {
            return context.getString(R.string.empty_amount_error_message)
        } else if (amount <= 0) {
            return context.getString(R.string.zero_amount_error_message)
        }

        return null
    }
}