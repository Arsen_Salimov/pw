package com.maetimo.pw.constants

class Constants {
    companion object {
        const val API_BASE_URL = "http://193.124.114.46:3001"
        const val MIN_PASSWORD_LENGTH = 6
    }
}